import axios from "axios"
import { BASE_URL, configHeaders, https } from "./config"

export const movieService = {
    getMovieList: () => {
        // return axios({
        //     url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01`,
        //     method: 'GET',
        //     headers: configHeaders()
        // })
        return https.get(`${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP05`)
    },

    getMovieTheater: () => {
        // return axios({
        //     url: `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap`,
        //     method: 'GET',
        //     headers: configHeaders()
        // })
        return https.get(`${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap`)
    },
    
    getDetailMovie: (maPhim) => {
        return https.get(`${BASE_URL}/api/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`)
    }
}