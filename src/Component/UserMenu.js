import React from 'react'
import { useSelector } from 'react-redux'
import { NavLink, useNavigate } from 'react-router-dom'
import { localUserServ } from '../service/localService'
import UserDropdown from './Header/UserDropdown'

export default function UserMenu() {
  const { userInfo } = useSelector(state => state.useReducer)
  const navigate = useNavigate()
  let hanndleLogout = () => {
    localUserServ.remove(userInfo)
    // c1
    // window.location.reload()
    // hoặc là đẩy lên reducer để reducer trả lại state update lại view
    navigate("/login")
  }

    const renderContent = () => {
      let buttonCss = "px-5 py-2 border-2 border-black rounded"
      if (userInfo) {
        return <div className='flex items-center'>
          <UserDropdown user={userInfo} logoutBtn={
           <button onClick={hanndleLogout} className={buttonCss}>Đăng xuất</button>
          }/>
        </div>
      } else {
        return <>
          <NavLink to='/login'>
            <button className={buttonCss}>Đăng Nhập</button>
          </NavLink>
          <NavLink>
            <button className={buttonCss}>Đăng Ký</button>
          </NavLink>

        </>
      }
    }
  return (
    <div>
      {renderContent()}
    </div>
  )
}
