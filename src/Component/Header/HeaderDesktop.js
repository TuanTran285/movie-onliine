import React from 'react'
import { NavLink } from 'react-router-dom'
import UserMenu from '../UserMenu'

export default function HeaderDesktop() {
  return (
    <div className='px-5 h-20 shadow w-full'>
        <div className='containe mx-auto h-full flex items-center justify-between'>
            <NavLink to="/" className='font-medium text-2xl text-red-500 animate-pulse'>CyberFlix</NavLink>
            <UserMenu />
        </div>
    </div>
  )
}
