import { DownOutlined } from '@ant-design/icons';
import { Dropdown, Space } from 'antd';
import { NavLink } from 'react-router-dom';
const UserDropdown = ({ user, logoutBtn }) => (
    <Dropdown
        menu={{
            items: [{ label: logoutBtn, key: "1" }]
        }}
        trigger={['click']}>
        <a onClick={(e) => e.preventDefault()}>
            <Space>
                {user.hoTen}
                <DownOutlined />
            </Space>
        </a>
    </Dropdown>
);
export default UserDropdown;