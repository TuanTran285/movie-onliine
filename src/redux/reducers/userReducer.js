import { localUserServ } from "../../service/localService"
import { USER_LOGIN } from "../constants/userContains"

const initialState = {
    userInfo: localUserServ.get()
}

const useReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_LOGIN: {
        state.userInfo = action.userInfo
        // localStorage
        return {...state}
    }

  default:
    return state
  }
}
export default useReducer