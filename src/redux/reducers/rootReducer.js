import {combineReducers} from 'redux'
import useReducer from './userReducer'
let rootReducer = combineReducers({
    useReducer
})
export default rootReducer