import { message } from "antd"
import { localUserServ } from "../../service/localService"
import { userServ } from "../../service/userService"
import { USER_LOGIN } from "../constants/userContains"

export const setLoginAction = (value) => {
    return {
        type: USER_LOGIN,
        userInfo: value
    }
}

export const setLoginActionService = (userForm, onCompleted) => {
    return async dispatch => {
        try {
            const result = await userServ.postLogin(userForm)
            if (result.status == 200) {
                dispatch({
                    type: USER_LOGIN,
                    userInfo: result.data.content
                })
                // Lưu thông tin vào localStorage
                localUserServ.set(result.data.content)
                onCompleted()
            }
        } catch (err) {
            console.log("err", err)
        }
    }
}