import React, { useEffect, useState } from 'react'
import { NavLink, useParams } from 'react-router-dom'
import { movieService } from '../../service/movieService'

export default function DetailPage() {
  const { id } = useParams()
  const [movie, setMovie] = useState([])

  useEffect(() => {
    let fetchDetail = async () => {
      try {
        let result = await movieService.getDetailMovie(id)
        setMovie(result.data.content)

      } catch (err) {
        console.log("err", err)
      }
    }
    fetchDetail()
  }, [])



  return (
    <div className='container'>
      <div className='flex space-x-10'>
        <img src={movie.hinhAnh} className="w-1/3" alt="" />
        <div className='space-y-5'>
        <h2>{movie.tenPhim}</h2>
          <NavLink to={`/booking/${id}`} >Mua vé</NavLink>
        </div>
      </div>
    </div>
  )
}
