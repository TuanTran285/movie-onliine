import React from 'react'
import { Button, Checkbox, Form, Input, message } from 'antd';
import { userServ } from '../../service/userService';
import { localUserServ } from '../../service/localService';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { USER_LOGIN } from '../../redux/constants/userContains';
import { setLoginAction, setLoginActionService } from '../../redux/action/userAction';
// import Lottie from "lottie-react";
// import bg_animate from "../../assets/aimate_login.json"


export default function LoginPage() {
    let navigate = useNavigate()
    let dispatch = useDispatch()
    const onFinish = (values) => {
        userServ.postLogin(values).then((res) => {
            message.success("Login thành công")
            // Lưu thông tin vào localStorage
            localUserServ.set(res.data.content)
            // chuyển hướng user tới homePage
            navigate('/')
            // đưa thông tin user lên reducer để lấy thông tin để render tài khoản khi đăng nhập thành công
            dispatch(setLoginAction(res.data.content))
            // console.log(res)
        })
            .catch((err) => {
                message.error('Login thất bại')
                console.log(err);
            });
    };

    const onFinishThunk = (values) => {
        let onSuccess = () => {
            message.success("Login thành công")
            // chuyển hướng user tới homePage
            navigate('/')
        }
        dispatch(setLoginActionService(values, onSuccess))
    }
    
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    return (
        <div className='h-screen w-screen flex items-center justify-center bg-orange-500'>
            <div className="container mx-auto p-5 bg-white rounded flex">
            <div className='w-1/2 h-full'>
            </div>
            <div className='w-1/2 h-full'>
                <Form
                    name="basic"
                    labelCol={{
                        span: 8,
                    }}
                    wrapperCol={{
                        span: 24,
                    }}
                    style={{
                        maxWidth: '100%',
                    }}
                    initialValues={{
                        remember: true,
                    }}
                    onFinish={onFinishThunk}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                    layout='vertical'
                >
                    <Form.Item
                        label="Username"
                        name="taiKhoan"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your username!',
                            },
                        ]}
                        la
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="Password"
                        name="matKhau"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your password!',
                            },
                        ]}
                    >
                        <Input.Password />
                    </Form.Item>
                    <Form.Item
                        wrapperCol={{
                            span: 24,
                        }}
                        className="flex items-center justify-center"
                    >
                        <Button className='bg-orange-500 text-white hover:text-white border-none' htmlType="submit">
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            </div>
            </div>
        </div>
    )
}
