import React from 'react'
import Header from '../../Component/Header'
import ListMovie from '../ListMovie/ListMovie'
import TabsMovie from '../ListMovie/TabsMovie'
export default function HomePage() {
  return (
    <div className='space-y-10'>
    <Header/>
    <ListMovie />
    <TabsMovie />
    </div>
  )
}
