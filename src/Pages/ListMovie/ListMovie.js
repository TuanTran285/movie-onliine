import React, { useEffect, useState } from 'react'
import { movieService } from '../../service/movieService'
import ItemMovie from './ItemMovie';
export default function ListMovie() {
    const [movies, setMovies] = useState([])
    useEffect(() => {
        movieService.getMovieList()
        .then((res) => {
            setMovies(res.data.content)
              })
              .catch((err) => {
               console.log(err);
              });
    }, [])
  return (
    <div className='container grid grid-cols-5 gap-10'>
        {movies.map((item) => {
            return <ItemMovie key={item.maPhim} data=
            {item}/>
        })}
    </div>
  )
}
