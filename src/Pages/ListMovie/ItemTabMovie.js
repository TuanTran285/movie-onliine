import React from 'react'
import moment from 'moment'
export default function ItemTabMovie({phim}) {
  return (
    <div className='p-5 space-x-10 flex'>
        <img src={phim.hinhAnh} className="w-28 h-36 rounded-xl" alt="" />
        <h3 className='font-medium text-xl'>{phim.tenPhim}</h3>
        <div className='grid grid-cols-3 gap-5'>{phim.lstLichChieuTheoPhim.slice(0, 9).map((item, index) => {
            return <span key={index} className='h-16 rounded p-2 bg-red-500 text-white font-medium'>{moment(item.ngayChieuGioChieu).format("DD/MM/YYYY ~ hh:mm")}</span>
        })}</div>
    </div>
  )
}
