import { Card } from 'antd'
import Meta from 'antd/es/card/Meta'
import React from 'react'
import { NavLink } from 'react-router-dom'

export default function ItemMovie({ data }) {
    return (
        <Card 
            hoverable
            cover={<img className='h-40 object-cover object-top' alt="example" src={data.hinhAnh} />}
        >
            <Meta className='h-20' title={data.tenPhim} description={<NavLink to={`/detail/${data.maPhim}`} className="px-5 py-2 bg-purple-600 rounded-lg text-white font-normal">Xem ngay</NavLink>} />
        </Card>
    )
}
