import React, { useEffect, useState } from 'react'
import { movieService } from '../../service/movieService'

import { Tabs } from 'antd';
import ItemMovie from './ItemMovie';
import ItemTabMovie from './ItemTabMovie';
const onChange = (key) => {
  console.log(key);
};

export default function TabsMovie() {
  const [heThongRap, setHeThongRap] = useState([])
  useEffect(() => {
    movieService.getMovieTheater()
      .then((res) => {
        setHeThongRap(res.data.content)
      })
      .catch((err) => {
        console.log(err);
      });
  }, [])

  let renderHeThongRap = () => {
    return heThongRap.map(rap => {
      return {
        key: rap.maHeThongRap,
        label: <img className='h-16' src={rap.logo} alt="" />,
        children: (
          <Tabs
            style={{ height: 600 }}
            tabPosition='left'
            defaultActiveKey="1" className='grid-cols-3 grid-flow-col' items={rap.lstCumRap.map(cumRap => {
              return {
                key: cumRap.tenCumRap,
                label: <div>{cumRap.tenCumRap}</div>,
                children: <div style={{height: 600}} className='overflow-y-scroll'>
                  {cumRap.danhSachPhim.map((item) => {
                    return <ItemTabMovie phim={item} />

                  })
                  }
                </div>
              }
            })} onChange={onChange} />
        )
      }
    })
  }
  return (
    <div className='container'>
      <Tabs tabPosition='left'
        defaultActiveKey="1" className='grid-cols-3 grid-flow-col' items={renderHeThongRap()} onChange={onChange} />
      <br /><br /><br /><br /> <br /><br /><br /><br /><br /><br />
    </div>
  )
}
