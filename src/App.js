import { BrowserRouter, Route, Routes } from "react-router-dom";
import Header from "./Component/Header";
import HomePage from "./Pages/HomePage/HomePage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import TabsMovie from "./Pages/ListMovie/TabsMovie";
import DetailPage from "./Pages/DetailPage/DetailPage";
import BookingPage from "./Pages/BookingPage/BookingPage";
import Layout from './Layout/Layout'
import NotFoundPage from "./Pages/NotFoundPage/NotFoundPage";
function App() {
  return (
    <div>
    {/* <BrowserRouter>
     được sử dụng để bao bọc toàn bộ ứng dụng React, để cho phép chuyển đổi giữa các trang và các đường dẫn URL mà không cần tải lại trang. */}
     {/* <Routes> được sử dụng để định nghĩa các tuyến đường (routes)
     cho ứng dụng React. Nó giúp ứng dụng định tuyến đến các thành phần (component) khác nhau tương ứng với đường dẫn URL tương ứng. */}
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HomePage/>}/>
          <Route path="/login" element={<LoginPage/>}/>
          <Route path="/detail/:id" element={<Layout Component={DetailPage} />}/>
          <Route path="/booking/:id" element={<Layout Component={BookingPage} />}/>
          <Route path="*" element={<Layout Component={NotFoundPage}/>}/>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
