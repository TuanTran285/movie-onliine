import React from 'react'
import Footer from '../Component/Footer/Footer'
import Header from '../Component/Header'

export default function layout({ Component }) {
  return (
    <div className='min-h-screen flex flex-col space-y-10'>
      <Header />
      <div className='flex-grow'>
        <Component />
      </div>
      <Footer />
    </div>
  )
}
